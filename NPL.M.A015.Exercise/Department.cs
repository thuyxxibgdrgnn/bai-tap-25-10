﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Department
    {
        public int DepartmentID { get; set; }
        public string Name { get; set; }
      

        public Department()
        {
        }

        public Department(int departmentID, string name)
        {
            DepartmentID = departmentID;
            Name = name;
            
        }
    }
}
