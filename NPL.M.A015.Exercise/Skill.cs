﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Skill
    {
        public int LanguageID { get; set; }
        public int EmployeeID { get; set; }

        public Skill()
        {
        }

        public Skill(int languageID, int employeeID)
        {
            LanguageID = languageID;
            EmployeeID = employeeID;
        }
    }
}
